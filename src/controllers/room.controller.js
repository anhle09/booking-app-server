const httpStatus = require('http-status');
const { roomService } = require('../services');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');

const createRoom = catchAsync(async (req, res) => {
  if (req.files?.length) {
    const imageList = req.files.map(file => ({ url: '/images/' + file.filename }));
    req.body.images = imageList;
    req.body.cover_image = imageList[0]?.url;
  }
  const room = await roomService.createRoom(req.body);
  res.status(httpStatus.CREATED).send({ data: room });
});

const getRooms = catchAsync(async (req, res) => {
  const { ward, district, province, host, min, max, cancel } = req.query;
  const filter = {
    'address.ward_id': ward,
    'address.district_id': district,
    'address.province_id': province,
    'host_id': host,
    'price.weekday': min && max && { $gte: min, $lt: max },
    'cancel_policy_id': cancel,
  };
  // delete undefined field
  Object.keys(filter).forEach(key => {
    if (filter[key] === undefined) delete filter[key]
  });

  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  const rooms = await roomService.queryRooms(filter, options);
  res.send(rooms);
});

const getRoom = catchAsync(async (req, res) => {
  const room = await roomService.getRoomById(req.params.roomId);
  if (!room) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Room not found');
  }
  res.send({ data: room });
});

const updateRoom = catchAsync(async (req, res) => {
  const roomData = await roomService.getRoomById(req.params.roomId);
  const oldImageList = roomData.images;

  if (req.files?.length) {
    const imageList = req.files.map(file => ({ url: '/images/' + file.filename }));
    req.body.images = [ ...oldImageList, ...imageList ];
  }
  const room = await roomService.updateRoomById(req.params.roomId, req.body);
  res.send({ data: room });
});

const deleteRoom = catchAsync(async (req, res) => {
  await roomService.deleteRoomById(req.params.roomId);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createRoom,
  getRooms,
  getRoom,
  updateRoom,
  deleteRoom,
};
