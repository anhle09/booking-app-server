module.exports.authController = require('./auth.controller');
module.exports.userController = require('./user.controller');
module.exports.roomController = require('./room.controller');
module.exports.reservationController = require('./reservation.controller');
module.exports.projectController = require('./project.controller');
module.exports.taskController = require('./task.controller');
