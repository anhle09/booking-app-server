const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { taskService } = require('../services');

const create = catchAsync(async (req, res) => {
  const document = await taskService.create(req.body);
  res.status(httpStatus.CREATED).send({ data: document });
});

const query = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['member', 'project']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  // options.populate = 'member,project';
  const docList = await taskService.query(filter, options);
  res.send(docList);
});

const updateById = catchAsync(async (req, res) => {
  const document = await taskService.updateById(req.params.id, req.body);
  res.send({ data: document });
});

const deleteById = catchAsync(async (req, res) => {
  await taskService.deleteById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  create,
  query,
  updateById,
  deleteById,
};
