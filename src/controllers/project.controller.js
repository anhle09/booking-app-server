const httpStatus = require('http-status');
const pick = require('../utils/pick');
const ApiError = require('../utils/ApiError');
const catchAsync = require('../utils/catchAsync');
const { projectService } = require('../services');

const create = catchAsync(async (req, res) => {
  const document = await projectService.create(req.body);
  res.status(httpStatus.CREATED).send({ data: document });
});

const query = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['members']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  // options.populate = 'members';
  const docList = await projectService.query(filter, options);
  res.send(docList);
});

const getById = catchAsync(async (req, res) => {
  const document = await projectService.getById(req.params.id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  res.send({ data: document });
});

const updateById = catchAsync(async (req, res) => {
  const document = await projectService.updateById(req.params.id, req.body);
  res.send({ data: document });
});

const deleteById = catchAsync(async (req, res) => {
  await projectService.deleteById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  create,
  query,
  getById,
  updateById,
  deleteById,
};

/* 
const addMember = catchAsync(async (req, res) => {
  const document = await projectService.addMember(req.params.id, req.body.memberId);
  res.send({ data: document });
});

const removeMember = catchAsync(async (req, res) => {
  const document = await projectService.removeMember(req.params.id, req.body.memberId);
  res.send({ data: document });
});
 */