const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const generateAuthToken = require('../utils/authToken')
const { authService, userService } = require('../services');

const register = catchAsync(async (req, res) => {
  const user = await userService.createUser(req.body);
  const tokens = await generateAuthToken(user);
  const data = { user, token: tokens.access.token };
  res.status(httpStatus.CREATED).send({ data });
});

const login = catchAsync(async (req, res) => {
  const { email, password } = req.body;
  const user = await authService.loginUser(email, password);
  const tokens = await generateAuthToken(user);
  const data = { user, token: tokens.access.token };
  res.send({ data });
});

const changePassword = catchAsync(async (req, res) => {
  const user = await authService.changePassword(req.body);
  res.send({ data: user });
});

/* 
const logout = catchAsync(async (req, res) => {
  await authService.logout(req.body.refreshToken);
  res.status(httpStatus.NO_CONTENT).send();
});
*/

module.exports = {
  register,
  login,
  changePassword,
  /* logout, */
};
