const httpStatus = require('http-status');
const pick = require('../utils/pick');
const catchAsync = require('../utils/catchAsync');
const { reservationService } = require('../services');

const createReservation = catchAsync(async (req, res) => {
  const reservation = await reservationService.createReservation(req.body);
  res.status(httpStatus.CREATED).send({ data: reservation });
});

const getReservations = catchAsync(async (req, res) => {
  const filter = pick(req.query, ['host', 'client']);
  const options = pick(req.query, ['sortBy', 'limit', 'page']);
  options.populate = filter.host ? 'room,client' : 'room';
  const reservations = await reservationService.queryReservations(filter, options);
  res.send(reservations);
});

const updateReservation = catchAsync(async (req, res) => {
  const reservation = await reservationService.updateReservationById(req.params.id, req.body);
  res.send({ data: reservation });
});

const deleteReservation = catchAsync(async (req, res) => {
  await reservationService.deleteReservationById(req.params.id);
  res.status(httpStatus.NO_CONTENT).send();
});

module.exports = {
  createReservation,
  getReservations,
  updateReservation,
  deleteReservation,
};
