const mongoose = require('mongoose');

const mongooseOpt = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};

const mongooseConnect = () => {
  mongoose.connect(process.env.MONGODB_URL, mongooseOpt)
  .then(() => console.log('> MongoDB connected'))
  .catch(err => console.log('> MongoDB error', err));
}

module.exports = mongooseConnect;
