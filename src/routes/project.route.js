const express = require('express');
const authenticateUser = require('../middlewares/authentication');
const projectController = require('../controllers/project.controller');
const router = express.Router();

router.use(authenticateUser);   //add middleware

router.route('/')
  .post(projectController.create)
  .get(projectController.query);

router.route('/:id')
  .get(projectController.getById)
  .patch(projectController.updateById)
  .delete(projectController.deleteById);

module.exports = router;
