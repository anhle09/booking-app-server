const express = require('express');
const roomController = require('../controllers/room.controller');
const uploaderMiddleware = require('../middlewares/imageUploader');
const router = express.Router();

router.route('/')
  .post(uploaderMiddleware, roomController.createRoom)
  .get(roomController.getRooms);

router.route('/:roomId')
  .get(roomController.getRoom)
  .patch(uploaderMiddleware, roomController.updateRoom)
  .delete(roomController.deleteRoom);

module.exports = router;
