const express = require('express');
const authenticateUser = require('../middlewares/authentication');
const userController = require('../controllers/user.controller');
const uploaderMiddleware = require('../middlewares/imageUploader');
const router = express.Router();

router.use(authenticateUser);   //add middleware

router.get('/currentUser', userController.getCurrentUser);

router.route('/')
  .post(userController.createUser)
  .get(userController.getUsers);

router.route('/:userId')
  .get(userController.getUser)
  .patch(uploaderMiddleware, userController.updateUser)
  .delete(userController.deleteUser);

module.exports = router;
