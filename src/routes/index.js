const express = require('express');
const authRoute = require('./auth.route');
const userRoute = require('./user.route');
const roomRoute = require('./room.route');
const reservationRoute = require('./reservation.route');
const projectRoute = require('./project.route');
const taskRoute = require('./task.route');

const router = express.Router();

router.use('/auth', authRoute);
router.use('/users', userRoute);
router.use('/rooms', roomRoute);
router.use('/reservation', reservationRoute);
router.use('/projects', projectRoute);
router.use('/tasks', taskRoute);

module.exports = router;
