const express = require('express');
const authenticateUser = require('../middlewares/authentication');
const taskController = require('../controllers/task.controller');
const router = express.Router();

router.use(authenticateUser);   //add middleware

router.route('/')
  .post(taskController.create)
  .get(taskController.query);

router.route('/:id')
  .patch(taskController.updateById)
  .delete(taskController.deleteById);

module.exports = router;
