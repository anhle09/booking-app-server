const express = require('express');
const authenticateUser = require('../middlewares/authentication');
const reservationController = require('../controllers/reservation.controller');
const router = express.Router();

router.use(authenticateUser);   //add middleware

router.route('/')
  .post(reservationController.createReservation)
  .get(reservationController.getReservations);

router.route('/:id')
  .patch(reservationController.updateReservation)
  .delete(reservationController.deleteReservation);

module.exports = router;
