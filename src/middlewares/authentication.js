const jwt = require('jsonwebtoken');
const httpStatus = require('http-status');
const catchAsync = require('../utils/catchAsync');
const ApiError = require('../utils/ApiError');
const { getUserById } = require('../services/user.service');

const getAuthTokenFromRequest = (req) => {
  const header = req.get('Authorization') || '';
  const [bearer, token] = header.split(' ');
  return bearer === 'Bearer' && token ? token : null;
};

const verifyToken = (token) => {
  try {
    const payload = jwt.verify(token, process.env.SECRET_KEY);
    if (Object.keys(payload).length) {
      return payload
    }
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Invalid Token');
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, error.message);
  }
};

const authenticateUser = catchAsync(async (req, _res, next) => {
  const token = getAuthTokenFromRequest(req);
  if (!token) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Please authenticate');
  }
  const userId = verifyToken(token).sub;
  if (!userId) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  const user = await getUserById(userId);
  if (!user) {
    throw new ApiError(httpStatus.FORBIDDEN, 'Forbidden');
  }
  req.currentUser = user;
  next();
});

module.exports = authenticateUser;
