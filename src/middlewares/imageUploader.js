const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
  destination: './public/images',
  filename: function(req, file, cb) {
    cb(null, Date.now() + path.extname(file.originalname));
  }
});

const uploaderMiddleware = multer({ storage: storage }).array('images', 12);

module.exports = uploaderMiddleware;
