const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const roomSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      required: true,
      trim: true,
    },
    address: {
      detail: String,
      location_lat: String,
      location_long: String,
      province_id: Number,
      district_id: Number,
      ward_id: Number,
      province: String,
      district: String,
      ward: String,
    },
    cover_image: String,
    host_id: String,
    images: [],
    amenities: [],
    price: {
      max_guest: Number,
      weekday: Number,
      weekend: Number,
    },
    cancel_policy_id: {
      type: Number,
      default: 1,
    },
    status: {
      type: String,
      default: 'public',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
roomSchema.plugin(toJSON);
roomSchema.plugin(paginate);

const Room = mongoose.model('Room', roomSchema);

module.exports = Room;
