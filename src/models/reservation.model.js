const mongoose = require('mongoose');
const { toJSON, paginate } = require('./plugins');

const reservationSchema = mongoose.Schema(
  {
    host: {
      type: String,
      required: true,
    },
    room: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Room',
      required: true,
    },
    client: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    anotherName: { type: String },
    anotherPhone: { type: String },
    startDate: {
      type: String,
      required: true,
    },
    endDate: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      default: 'pending',
    },
  },
  {
    timestamps: true,
  }
);

// add plugin that converts mongoose to json
reservationSchema.plugin(toJSON);
reservationSchema.plugin(paginate);

const Reservation = mongoose.model('Reservation', reservationSchema);

module.exports = Reservation;
