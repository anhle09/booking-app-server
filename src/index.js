/* eslint-disable no-unused-vars */
const express = require('express');
const cors = require('cors');
const dotenv = require('dotenv')
const path = require('path')
const mongooseConnect = require('./config/mongoose');
const routes = require('./routes');

dotenv.config();
const port = process.env.PORT || 5000;
const app = express();
mongooseConnect();

app.use(express.json());
app.use(express.urlencoded());
app.use(cors());


app.use('/booking-app', express.static(path.join(__dirname, '..', 'booking-app-client')));
app.get('/booking-app*', function (req, res) {
  res.sendFile(path.join(__dirname, '..', 'booking-app-client', 'index.html'));
});

app.use('/management-app', express.static(path.join(__dirname, '..', 'management-app-client')));
app.get('/management-app*', function (req, res) {
  res.sendFile(path.join(__dirname, '..', 'management-app-client', 'index.html'));
});


app.use(express.static('./public'));
app.use('/api', routes);

app.use('/', (req, res) => {
  res.redirect('/booking-app');
});

// catch 404
app.use((req, res) => {
  res.status(404).send({error: {statusCode: 404, message: 'Not found'}});
});

// error handler
app.use((err, req, res, next) => {
  const { statusCode, message } = err;
  res.status(statusCode || 500).send({error: { statusCode, message }});
});

app.listen(port, () => console.log(`> Server started on port ${port}`));
