const httpStatus = require('http-status');
const { Reservation } = require('../models');
const ApiError = require('../utils/ApiError');

const createReservation = async (body) => {
  return Reservation.create(body);
};

const queryReservations = async (filter, options) => {
  const docList = await Reservation.paginate(filter, options);
  return docList;
};

const getReservationById = async (id) => {
  return Reservation.findById(id);
};

const updateReservationById = async (id, body) => {
  const document = await getReservationById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  Object.assign(document, body);
  await document.save();
  return document;
};

const deleteReservationById = async (id) => {
  const document = await getReservationById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await document.remove();
  return document;
};

module.exports = {
  createReservation,
  queryReservations,
  getReservationById,
  updateReservationById,
  deleteReservationById,
};
