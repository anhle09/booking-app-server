module.exports.authService = require('./auth.service');
module.exports.userService = require('./user.service');
module.exports.roomService = require('./room.service');
module.exports.reservationService = require('./reservation.service');
module.exports.projectService = require('./project.service');
module.exports.taskService = require('./task.service');
