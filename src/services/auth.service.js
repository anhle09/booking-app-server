const httpStatus = require('http-status');
const userService = require('./user.service');
const ApiError = require('../utils/ApiError');

const loginUser = async (email, password) => {
  const user = await userService.getUserByEmail(email);
  const matched = await user.isPasswordMatch(password)
  if (!user || !matched) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect email or password');
  }
  return user;
};

const changePassword = async (body) => {
  const { email, password, newPassword } = body
  const user = await userService.getUserByEmail(email);
  const matched = await user.isPasswordMatch(password)
  if (user && matched) {
    Object.assign(user, { password: newPassword });
    await user.save();
  }
  else {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Incorrect password');
  }
  return user;
};

/* 
const logout = async (refreshToken) => {
  const refreshTokenDoc = await Token.findOne({ token: refreshToken, type: tokenTypes.REFRESH, blacklisted: false });
  if (!refreshTokenDoc) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await refreshTokenDoc.remove();
};

const resetPassword = async (resetPasswordToken, newPassword) => {
  try {
    const resetPasswordTokenDoc = await tokenService.verifyToken(resetPasswordToken, tokenTypes.RESET_PASSWORD);
    const user = await userService.getUserById(resetPasswordTokenDoc.user);
    if (!user) {
      throw new Error();
    }
    await userService.updateUserById(user.id, { password: newPassword });
    await Token.deleteMany({ user: user.id, type: tokenTypes.RESET_PASSWORD });
  } catch (error) {
    throw new ApiError(httpStatus.UNAUTHORIZED, 'Password reset failed');
  }
};
*/

module.exports = {
  loginUser,
  changePassword,
  /* logout,
  resetPassword, */
};
