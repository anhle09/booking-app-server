const httpStatus = require('http-status');
const { Task } = require('../models');
const ApiError = require('../utils/ApiError');

const create = async (body) => {
  return Task.create(body);
};

const query = async (filter, options) => {
  const docList = await Task.paginate(filter, options);
  return docList;
};

const getById = async (id) => {
  return Task.findById(id);
};

const updateById = async (id, body) => {
  const document = await getById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  Object.assign(document, body);
  await document.save();
  return document;
};

const deleteById = async (id) => {
  const document = await getById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await document.remove();
  return document;
};

module.exports = {
  create,
  query,
  getById,
  updateById,
  deleteById,
};
