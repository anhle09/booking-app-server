const httpStatus = require('http-status');
const { Project } = require('../models');
const ApiError = require('../utils/ApiError');

const create = async (body) => {
  return Project.create(body);
};

const query = async (filter, options) => {
  const docList = await Project.paginate(filter, options);
  return docList;
};

const getById = async (id) => {
  return Project.findById(id);
};

const updateById = async (id, body) => {
  const document = await getById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  Object.assign(document, body);
  await document.save();
  return document;
};

const deleteById = async (id) => {
  const document = await getById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  await document.remove();
  return document;
};

module.exports = {
  create,
  query,
  getById,
  updateById,
  deleteById,
};

/* 
const addMember = async (id, memberId) => {
  const document = await getById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  document.member.push(memberId)
  await document.save();
  return document;
};

const removeMember = async (id, memberId) => {
  const document = await getById(id);
  if (!document) {
    throw new ApiError(httpStatus.NOT_FOUND, 'Not found');
  }
  document.member.pull(memberId)
  await document.save();
  return document;
};
 */