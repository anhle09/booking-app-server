const jwt = require('jsonwebtoken');
const moment = require('moment');

const generateToken = (userId, expires, type) => {
  const payload = {
    sub: userId,
    iat: moment().unix(),
    exp: expires.unix(),
    type,
  };
  return jwt.sign(payload, process.env.SECRET_KEY);
};

const generateAuthToken = async (user) => {
  const accessTokenExpires = moment().add(30, 'days');
  const accessToken = generateToken(user.id, accessTokenExpires, 'access');

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toDate(),
    }
  };
};

module.exports = generateAuthToken;
